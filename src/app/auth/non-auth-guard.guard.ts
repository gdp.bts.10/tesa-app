import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../module/auth/login/services/api.service';

@Injectable({
  providedIn: 'root',
})
export class NonAuthGuardGuard implements CanActivate {
  constructor(private apiService: ApiService, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.apiService.getAccessToken()) {
      this.router.navigate(['dashboard']);
      return false;
    } else {
      return true;
    }
  }
}
