import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductServiceService } from './services/product-service.service';
import { ProductInterface } from './interface/product-interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class ContentComponent implements OnInit {
  product!: ProductInterface;
  constructor(
    private router: Router,
    private productService: ProductServiceService
  ) {}

  ngOnInit(): void {
    this.getAllProduct();
    console.log(this.product);
  }

  logout() {
    localStorage.removeItem('access_token');
    this.router.navigate(['auth/login']);
  }

  getAllProduct() {
    this.productService.getAllProduct().subscribe((data) => {
      console.log(data);
    });
  }
}
