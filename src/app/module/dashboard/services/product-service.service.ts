import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductInterface } from '../interface/product-interface';

@Injectable({
  providedIn: 'root',
})
export class ProductServiceService {
  apiUrl: string = 'http://private-e5e02c-newspages.apiary-mock.com/';
  httpOptionsPlain = {
    headers: new HttpHeaders({
      Accept: 'text/plain',
      'Content-Type': 'text/plain',
    }),
    responseType: 'text',
  };
  constructor(private httpClient: HttpClient) {}

  getAllProduct() {
    return this.httpClient.get(this.apiUrl + 'api/products');
  }
}
