export interface ProductInterface {
  id: number;
  name: string;
  price: number;
  imageUrl: string;
}
