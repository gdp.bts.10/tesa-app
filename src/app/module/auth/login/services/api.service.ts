import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserInterface } from '../interfaces/user-interface';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  isAuthenticated = false;
  apiUrl: string = 'http://private-e5e02c-newspages.apiary-mock.com';

  constructor(private httpClient: HttpClient, private router: Router) {}

  login(user: UserInterface) {
    return this.httpClient
      .post(this.apiUrl + '/auth/login', user)
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res.data.token);
        this.isAuthenticated = true;
        this.router.navigate(['dashboard/v1']);
      });
  }

  getAccessToken() {
    return localStorage.getItem('access_token');
  }
}
