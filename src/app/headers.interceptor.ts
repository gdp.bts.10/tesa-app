import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './module/auth/login/services/api.service';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {
  constructor(private apiService: ApiService) {}
  token = this.apiService.getAccessToken();
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const token = this.apiService.getAccessToken();
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer${token}`,
      },
    });
    return next.handle(request);
  }
}
