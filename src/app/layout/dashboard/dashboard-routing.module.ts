import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ContentComponent } from 'src/app/module/dashboard/dashboard.component';
import { AuthGuardGuard } from '../../auth/auth-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: 'v1', pathMatch: 'full' },
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'v1',
        component: ContentComponent,
        canActivate: [AuthGuardGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
